# Evaluation Cloud Computing

Ce projet met en place un web service de gestion de villes/sejours.

## Get started

Clonez le repo si ce n'est déjà fait:

- `git clone https://gitlab.com/guillaumeLamanda/evaluation-nodejs-m2`.

Rendez vous dans le répertoire, et installez les paquets :

- `npm i` si vous utilisez npm,
- `yarn` si vous utilisez yarn.

Lancez le serveur :

- `npm run dev` si vous utiliez npm,
- `yarn dev` si vous utilisez yarn.

## Technologies utilisés

- Express
- Sequelize (ORM mysql)
  - BDD sqlite
- ESM (pour ES modules)
  - Ce module permet d'utiliser la synthaxe es6+. Il est spécifié au lancement du serveur par enregistrement de module (`node -r esm lefichier.js`).

## DB

La base de donnée est initialisé dans le fichier `index` du dossier `db`.
J'utilise `sequelize` (ORM MYSQL) pour définir mes modèles, ainsi que mes associations.  
J'utilise, par convénience, une base de donnée sqlite. Elle se trouve ainsi dans un fichier db.sqlite, qui se trouve à la racine, et est créer par défaut au lancement du serveur.

## Serveur

Le serveur est défini dans le fichier index.js.  
J'y initialise les différents middlewares (body-parser, logger), ainsi que les routes.

Ces dernières se trouvent dans le dossier `routes`.  
J'y défini pour chacunes les méthodes GET et POST pour la route (exemple : `/ville`), et les methodes GET, PUT et DELETE pour la route contenant l'id (exemple `/ville/:id`).

Les différents fichiers sont commentés.

## Requêtes

A la racine, se trouve un fichier `request.rest`. Ce fichier est fait pour être ouvert sous VSCODE avec l'extension REST. Il permet de définir des requêtes à envoyer, un peut de la même manière que postman. Il est aussi possible de réccuperer des valeurs depuis les réponses des requêtes précédentes. C'est ce que j'ai réaliser. Le fichier est également commenté.
